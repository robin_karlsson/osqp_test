#include <iostream>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/LU>
#include <eigen3/Eigen/SparseCore>
#include <eigen3/Eigen/Cholesky>
#include <vector>
#include <tuple>

//#include "matrix_auxil.h"
#include "osqp_interface.h"

int main(int argc, char* argv[])
{
    // 'P' matrix:
    //   | 1  -1 |
    //   | -1  2 |
    Eigen::MatrixXd P(2,2);
    P << 1, -1, -1, 2;
    
    // 'q' vector:
    //   | -2 |
    //   | -6 |
    std::vector<double> q{-2, -6};

    // 'l' vector:
    //   | -1000 |
    //   | -1000 |
    std::vector<double> l{-1000, -1000, -1000};

    // 'A' vector:
    //   |  1  1 |
    //   | -1  2 |
    //   |  2  1 |
    Eigen::MatrixXd A(3,2);
    A << 1, 1, -1, 2, 2, 1;

    // 'u' vector:
    //   | 2 |
    //   | 2 |
    //   | 3 |
    std::vector<double> u{2, 2, 3};

    // Solve problem
    std::tuple<std::vector<double>, std::vector<double>> result = osqp::optimize(P, A, q, l, u);

    // Output solution
    std::vector<double> param = std::get<0>(result);
    for(std::vector<double>::const_iterator it = param.begin(); it != param.end(); it++){
        std::cout << *it << std::endl;
    }

    return 0;

}