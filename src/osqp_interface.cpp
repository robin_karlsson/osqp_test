/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: Robin Karlsson
 */

#include "osqp_interface.h"
#include <iostream>

namespace osqp
{
inline bool isEqual(double x, double y)
{
  const double epsilon = 1e-6;
  return std::abs(x - y) <= epsilon * std::abs(x);
}

std::tuple<std::vector<double>, std::vector<double>> optimize(const Eigen::MatrixXd P, const Eigen::MatrixXd A,
                                                            const std::vector<double> q, const std::vector<double> l,
                                                            const std::vector<double> u)
{
  /*******************
   * SET UP MATRICES
   *******************/
  // Transform 'P' into an 'upper trapesoidal matrix'
  Eigen::MatrixXd P_trap = P.triangularView<Eigen::Upper>();
  // CSC matrices
  CSC_Matrix P_csc = convEigenMatrixToCSCMatrix(P_trap);
  CSC_Matrix A_csc = convEigenMatrixToCSCMatrix(A);
  // Dynamic float arrays
  std::vector<double> q_tmp(q.begin(), q.end());
  std::vector<double> l_tmp(l.begin(), l.end());
  std::vector<double> u_tmp(u.begin(), u.end());
  double* q_dyn = q_tmp.data();
  double* l_dyn = l_tmp.data();
  double* u_dyn = u_tmp.data();

  /**********************
   * OBJECTIVE FUNCTION
   **********************/
  // Number of constraints
  c_int constr_m = A.rows();
  // Number of parameters
  c_int param_n = P_trap.rows();
  // Number of elements
  c_int P_elem_N = P_trap.size();
  c_int A_elem_N = A.size();

  // Exitflag
  c_int exitflag = 0;

  // Workspace structures
  OSQPWorkspace *work;
  OSQPSettings  *settings = (OSQPSettings *)c_malloc(sizeof(OSQPSettings));
  OSQPData      *data     = (OSQPData *)c_malloc(sizeof(OSQPData));

  // Populate data
  if(data)
  {
    data->m = constr_m;
    data->n = param_n;
    data->P = csc_matrix(data->n, data->n, P_elem_N, P_csc.elem_val.data(), P_csc.row_idx.data(), P_csc.col_idx.data());
    data->q = q_dyn;
    data->A = csc_matrix(data->m, data->n, A_elem_N, A_csc.elem_val.data(), A_csc.row_idx.data(), A_csc.col_idx.data());
    data->l = l_dyn;
    data->u = u_dyn;
  }

  /************
   * OPTIMIZE
   ************/
  // Define solver settings as default
    if (settings) {
        osqp_set_default_settings(settings);
        settings->alpha = 1.0; // Change alpha parameter
    }

  // Setup workspace
  exitflag = osqp_setup(&work, data, settings);

  // Solve Problem
  osqp_solve(work);

  /********************
   * EXTRACT SOLUTION
   ********************/
  double* sol_x = work->solution->x;
  double* sol_y = work->solution->y;
  std::vector<double> sol_primal(sol_x, sol_x + static_cast<int>(param_n));
  std::vector<double> sol_lagrange_multiplier(sol_y, sol_y + static_cast<int>(param_n));
  // Result tuple
  std::tuple<std::vector<double>, std::vector<double>> result = std::make_tuple(sol_primal, sol_lagrange_multiplier);

  /***********
   * CLEANUP
   ***********/
  if (data) {
        if (data->A) c_free(data->A);
        if (data->P) c_free(data->P);
        c_free(data);
    }
  if (settings) c_free(settings);

  return result;
}

CSC_Matrix convEigenMatrixToCSCMatrix(const Eigen::MatrixXd A)
{
  // Input dense matrix dimensions
  int A_rows = A.rows();
  int A_cols = A.cols();

  /************************************************************************
   * Generate 'sparse matrix B' from nonzero elements in 'dense matrix A'
   ************************************************************************/

  // Generate list of nonzero elements
  std::vector<Eigen::Triplet<double>> triplet_list;
  triplet_list.reserve(A.size());
  for (int i = 0; i < A_rows; i++)
  {
    for (int j = 0; j < A_cols; j++)
    {
      double A_val = A(i, j);
      if (!isEqual(A_val, 0.0))
      {
        triplet_list.push_back(Eigen::Triplet<double>(i, j, A_val));
      }
    }
  }
  // Generate 'sparse matrix B' and fill with nonzero elements in list
  Eigen::SparseMatrix<double> B(A_rows, A_cols);
  B.setFromTriplets(triplet_list.begin(), triplet_list.end());

  /************************************************************************
   * Generate 'Compressed Sparse Column (CSC) Matrix A_csc' struct object
   ************************************************************************/

  // Generate CSC matrix B
  int B_nonzero_N = B.nonZeros();
  B.makeCompressed();

  // Extract pointer arrays
  double* val_ptr = B.valuePtr();
  int* inn_ptr = B.innerIndexPtr();
  int* out_ptr = B.outerIndexPtr();

  // Copy values of pointer arrays into vectors in CSC struct
  // Array lengths:
  //     elem_val : nonzero element count
  //     row_idx  : nonzero element count
  //     col_idx  : input matrix column count + 1
  CSC_Matrix A_csc;
  A_csc.elem_val.assign(val_ptr, val_ptr + B_nonzero_N);
  A_csc.col_idx.assign(out_ptr, out_ptr + A_cols + 1);
  A_csc.row_idx.assign(inn_ptr, inn_ptr + B_nonzero_N);

  return A_csc;
}

}  // namespace osqp
