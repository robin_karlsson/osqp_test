cmake_minimum_required(VERSION 2.8.3)
project(osqp_test)

add_compile_options(-std=c++11)

include_directories(
    include
    lib/osqp/include
)

add_executable(osqp_test
    src/main.cpp
    src/osqp_interface.cpp
)

target_link_libraries(osqp_test
    ${CMAKE_SOURCE_DIR}/lib/osqp/lib/libosqp.a
    ${CMAKE_DL_LIBS}
)